package star.lut.readexcell;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.Toast;

import com.github.tutorialsandroid.filepicker.controller.DialogSelectionListener;
import com.github.tutorialsandroid.filepicker.model.DialogConfigs;
import com.github.tutorialsandroid.filepicker.model.DialogProperties;
import com.github.tutorialsandroid.filepicker.view.FilePickerDialog;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Timer;

public class MainActivity extends AppCompatActivity {
    List<DataModel> rowList;
    DataModel columnName;
    RecyclerView rvSearchResult;
    AutoCompleteTextView autoCompleteTextView;
    Button btnSelectFile;
    ConstraintLayout clFileChooser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        rowList = new ArrayList<>();

        autoCompleteTextView = findViewById(R.id.autoCompleteTextView);
        rvSearchResult = findViewById(R.id.rvSearchResult);
        btnSelectFile = findViewById(R.id.btnSelectFile);
        clFileChooser = findViewById(R.id.clFileChooser);

//        initView();
        if (checkPermissions()) {
            selectFile();
        }else{
            requestPermissions();
        }
    }

    private boolean checkPermissions() {
        if (PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)){
            return true;
        }else {
            return false;
        }
    }

    private void selectFile(){
        btnSelectFile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogProperties properties = new DialogProperties();
                properties.selection_mode = DialogConfigs.SINGLE_MODE;
                properties.selection_type = DialogConfigs.FILE_SELECT;
                properties.root = new File(DialogConfigs.DEFAULT_DIR);
                properties.error_dir = new File(DialogConfigs.DEFAULT_DIR);
                properties.offset = new File(DialogConfigs.DEFAULT_DIR);
                properties.extensions = null;
                FilePickerDialog dialog = new FilePickerDialog(MainActivity.this,properties);
                dialog.setTitle("Select a File");
                dialog.setDialogSelectionListener(new DialogSelectionListener() {
                    @Override
                    public void onSelectedFilePaths(String[] files) {
                        //files is the array of the paths of files selected by the Application User.
                        if (files.length > 0){
                            Log.d("zzzzz", files[0]);
                            if (files[0].contains("xlsx")){
                                initData(files[0]);
                            }
                        }
                    }
                });
                dialog.show();
            }
        });
    }

    public void initView(){
        clFileChooser.setVisibility(View.GONE);
        rvSearchResult.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(rvSearchResult.getContext(),
                linearLayoutManager.getOrientation());
        rvSearchResult.addItemDecoration(dividerItemDecoration);
        rvSearchResult.setLayoutManager(linearLayoutManager);

        SearchNumberAutoCompleteAdapter adapter = new SearchNumberAutoCompleteAdapter(rowList, this, new SearchNumberAutoCompleteAdapter.OnItmeClickListner() {
            @Override
            public void onClick(DataModel dataModel) {
                Log.d("adapterSetting", columnName.toString());
                RvSearchResult result = new RvSearchResult(dataModel, columnName, MainActivity.this);
                rvSearchResult.setAdapter(result);
            }
        });

        autoCompleteTextView.setAdapter(adapter);
    }

    public void initData(String filePath){
        System.setProperty("org.apache.poi.javax.xml.stream.XMLInputFactory", "com.fasterxml.aalto.stax.InputFactoryImpl");
        System.setProperty("org.apache.poi.javax.xml.stream.XMLOutputFactory", "com.fasterxml.aalto.stax.OutputFactoryImpl");
        System.setProperty("org.apache.poi.javax.xml.stream.XMLEventFactory", "com.fasterxml.aalto.stax.EventFactoryImpl");

        try {
            InputStream inputStream = new DataInputStream(new FileInputStream(new File(filePath)));
            Workbook wb = WorkbookFactory.create(inputStream);

            int i = 0;
            Iterator wbIterator = wb.iterator();
            while (wbIterator.hasNext()){
                wbIterator.next();
                Sheet sheet = wb.getSheetAt(i);
                Iterator sheetIterator = sheet.iterator();
                int j = 0;
                while (sheetIterator.hasNext()){
                    DataModel dataModel = new DataModel();
                    sheetIterator.next();
                    Row row = sheet.getRow(j);
                    Iterator rowIteraotr = row.iterator();
                    int k = 0;
                    while (rowIteraotr.hasNext()){
                        if (row.getCell(k) != null){
                            dataModel.strings.add(row.getCell(k).toString());
                        }
                        rowIteraotr.next();
                        k++;
                    }
                    j++;

                    Log.d("yzd",dataModel.strings.toString());
                    rowList.add(dataModel);
                }
                i++;
            }

            Log.d("initData", rowList.toString());

            columnName = rowList.get(0);

            refactorPhone();

//            Row row = wb.getSheetAt(0).getRow(0);
//            row.iterator().hasNext();
//            Log.d("xyz", row.toString());
//
//            Log.d("xyz", row.getRowNum()+"");
//            while (row.iterator().hasNext()){
//                Log.d("xyz", row.getCell(i).getStringCellValue());
//                i++;
//            }

            initView();
        } catch (Exception e) {
            Toast.makeText(this, "Please select valid file type", Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
    }

    public void refactorPhone(){
        int pos = getPhoneNumberPos();
        Log.d("refactorPhone", pos+"");
        for (int i = 0 ; i < rowList.size() ; i++){
            String temp = rowList.get(i).strings.get(pos);
            temp = temp.replace(".","");
            temp = temp.replace("E9","");
            rowList.get(i).strings.set(pos, temp);
            Log.d("qqq", rowList.get(i).strings.get(pos));
        }

        Log.d("refactorPhone", rowList.toString());
    }

    int getPhoneNumberPos(){
        int pos = 0;
        Log.d("getPhoneNumberPos",rowList.toString());
        for (int i = 0 ; i<columnName.strings.size() ; i++){
            if (columnName.strings.get(i).toLowerCase().contains("Agent Wallet No".toLowerCase())){
                Log.d("getPhoneNumberPos", columnName.strings.get(i));
                pos = i;
                return pos;
            }
        }

        return pos;
    }

    private void requestPermissions() {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                    123);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode == 123) {
            Log.d("request length",grantResults.length + "");
            if (grantResults.length <= 0) {
                // If user interaction was interrupted, the permission request is cancelled and you
                // receive empty arrays.
                Log.d("request canceled","User interaction was cancelled.");
            } else if (grantResults[0] == PackageManager.PERMISSION_GRANTED ) {
                selectFile();
                Log.d("request canceled","granted");
            } else {
                Toast.makeText(this, "Please give storage permission from settings to continue", Toast.LENGTH_LONG).show();
                Log.d("requestpermissionresult","permission denied");
            }
        }
    }
}
