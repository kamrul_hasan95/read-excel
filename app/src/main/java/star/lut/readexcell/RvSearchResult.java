package star.lut.readexcell;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class RvSearchResult extends RecyclerView.Adapter<RvSearchResult.ViewHolderResult> {
    public DataModel rowData;
    public DataModel rowName;
    private Context context;

    public RvSearchResult(DataModel rowData, DataModel rowName, Context context) {
        this.rowData = rowData;
        this.rowName = rowName;
        Log.d("RvSearchResult", rowName.strings.size()+"");
        this.context = context;
    }

    @Override
    public int getItemCount() {
        return rowData.strings.size();
    }

    class ViewHolderResult extends RecyclerView.ViewHolder{
        TextView tvColumnName;
        TextView tvColumnValue;
        public ViewHolderResult(@NonNull View itemView) {
            super(itemView);
            tvColumnName= (TextView)itemView.findViewById(R.id.tvColumnName);
            tvColumnValue = (TextView)itemView.findViewById(R.id.tvColumnValue);
        }
    }

    @NonNull
    @Override
    public ViewHolderResult onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_result, parent, false);
        return new ViewHolderResult(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderResult holder, int position) {
        String temp = rowData.strings.get(position);
        if (temp.contains("E9") && temp.contains(".")) {
            temp = temp.replace(".", "");
            temp = temp.replace("E9", "");
            rowData.strings.set(position, temp);
        }

        holder.tvColumnName.setText(rowName.strings.get(position));
        holder.tvColumnValue.setText(rowData.strings.get(position));
    }


}
