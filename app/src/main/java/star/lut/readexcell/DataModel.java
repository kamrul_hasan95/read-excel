package star.lut.readexcell;

import java.util.ArrayList;
import java.util.List;

public class DataModel {
    List<String> strings;

    public DataModel() {
        strings = new ArrayList<>();
    }

    @Override
    public String toString() {
        return "DataModel{" +
                "strings=" + strings +
                '}';
    }
}
