package star.lut.readexcell;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class SearchNumberAutoCompleteAdapter extends ArrayAdapter<DataModel> {
    List<DataModel> dataModels;
    Context context;
    OnItmeClickListner listner;
    int phoneIndex;

    public SearchNumberAutoCompleteAdapter( List<DataModel> dataModels, Context context, OnItmeClickListner onItmeClickListner) {
        super(context, 0, dataModels);
        this.dataModels = new ArrayList<>(dataModels);
        this.context = context;
        listner = onItmeClickListner;
        getPhoneRow();
        Log.d("yyy", "cons"+dataModels);
    }

    interface OnItmeClickListner{
        void onClick(DataModel dataModel);
    }

    private Filter mFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            Log.d("yyy", "performfiltering "+constraint);


            FilterResults results = new FilterResults();
            List<DataModel> suggestion = new ArrayList<>();
            Log.d("yyy", "performfiltering "+dataModels.toString());

            if (constraint == null || constraint.length() == 0){

            }else {
                Log.d("yyy", "performfiltering "+dataModels.toString());
                String filterPattern = constraint.toString().trim();

                for (DataModel model : dataModels) {

                    Log.d("yyy", "performfiltering "+model.toString());
                    if (model.strings.get(phoneIndex).contains(filterPattern)){
                        suggestion.add(model);
                    }
                }
            }

            results.values = suggestion;
            results.count = suggestion.size();
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            clear();
            addAll((List)results.values);
            notifyDataSetChanged();
        }

        @Override
        public CharSequence convertResultToString(Object resultValue) {
            return ((DataModel) resultValue).strings.get(phoneIndex);
        }
    };

    void getPhoneRow(){
        Log.d("yyy",dataModels.toString());
        for (int i = 0 ; i<dataModels.get(0).strings.size() ; i++){
            if (dataModels.get(0).strings.get(i).toLowerCase().contains("Agent Wallet No".toLowerCase())){
                Log.d("yyy", dataModels.get(0).strings.get(i));
                phoneIndex = i;
                return;
            }
        }
    }

    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if (convertView == null){
            convertView = LayoutInflater.from(getContext()).inflate(
                    R.layout.item_search_result, parent, false
            );
        }

        TextView tvName = convertView.findViewById(R.id.tvPhone);
        tvName.setText(getItem(position).strings.get(0));

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listner.onClick(getItem(position));
                clear();
            }
        });

        return convertView;
    }

    @Override
    public Filter getFilter() {
        return mFilter;
    }
}
